package com.example.dell;

import android.content.ClipData;
import android.content.Context;
import android.content.Intent;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {
    private List<news>newsList=new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initNews();
        RecyclerView recyclerView=(RecyclerView)findViewById(R.id.recycler_view);
        LinearLayoutManager linearLayoutManager=new LinearLayoutManager(this);
        recyclerView.setLayoutManager(linearLayoutManager);
        newsAdapter adapter=new newsAdapter(newsList);
        recyclerView.setAdapter(adapter);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        Button toeasyLife=(Button)findViewById(R.id.bmfu_button);
        toeasyLife.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent easylife=new Intent(MainActivity.this,EasyLifeActivity.class);
                startActivity(easylife);
            }
        });

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });

        //Button toEasylife=(Button)findViewById(R.id.bmfu_button);
        //toEasylife.setOnClickListener(new View.OnClickListener() {
        //    @Override
        //    public void onClick(View v) {
        //        Intent easyLife=new Intent(MainActivity.this,EasyLifeActivity.class);
        //        startActivity(easyLife);
        //    }
        //});

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
    }
    private  void initNews(){
        for(int i=0;i<5;i++){
            news news1=new news("武科大1号路路灯失修损坏，11111111111111111111111","2017年12月9日","武汉科技大学",R.drawable.ic_android_black_24dp);
            newsList.add(news1);
        }
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main,menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        switch (item.getItemId()){
            case R.id.action_putOn:
                Intent toPut=new Intent(MainActivity.this,putActivity.class);
                startActivity(toPut);
                break;

            default:
        }
       return true;
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_camera) {
            // Handle the camera action
        } else if (id == R.id.nav_gallery) {

        } else if (id == R.id.nav_slideshow) {

        } else if (id == R.id.nav_manage) {

        } else if (id == R.id.nav_share) {

        } else if (id == R.id.nav_send) {

        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
}

class news{
    private int imageid;
    private String newsdescribe;
    private String time;
    private String address;
    public news(String newsdescribe,String time,String address,int imageid){
        this.newsdescribe=newsdescribe;
        this.imageid=imageid;
        this.time=time;
        this.address=address;
    }
    public String getNewsdescribe(){
        return newsdescribe;
    }
    public int getImageid(){
        return imageid;
    }
    public String getTime(){ return time; }
    public String getAddress(){
        return address;
    }
}
class newsAdapter extends RecyclerView.Adapter<newsAdapter.ViewHolder>{
    private List<news> newsList;
    static class ViewHolder extends RecyclerView.ViewHolder{

        ImageView newsImage;
        TextView newsDescribe;
        TextView newstime;
        TextView newsAddress;
        public ViewHolder(View view){
            super(view);
            newsImage=(ImageView)view.findViewById(R.id.image_set);
            newsDescribe=(TextView)view.findViewById(R.id.describe_set);
            newstime=(TextView)view.findViewById(R.id.time_set);
            newsAddress=(TextView)view.findViewById(R.id.address_set);
        }
    }
    public newsAdapter(List<news> newsList){
        this.newsList=newsList;
    }
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent,int viewType){
        View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.recycler,parent,false);
        ViewHolder holder=new ViewHolder(view);
        return holder;
    }
    @Override
    public void onBindViewHolder(ViewHolder holder,int position){
        news news1=newsList.get(position);
        holder.newsImage.setImageResource(news1.getImageid());
        holder.newsDescribe.setText(news1.getNewsdescribe());
        holder.newstime.setText(news1.getTime());
        holder.newsAddress.setText(news1.getAddress());
    }
    @Override
    public int getItemCount(){
    return newsList.size();
    }
}
