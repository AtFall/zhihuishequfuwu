package com.example.dell;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;

public class EasyLifeActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_easy_life);
        ImageButton tojdz=(ImageButton)findViewById(R.id.jdz_button);
        tojdz.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent toPutOn =new Intent(EasyLifeActivity.this,PutOnActivity.class);
                startActivity(toPutOn);
            }
        });
    }
}
